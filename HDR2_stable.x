#include "PhysicsList.hh"
#include "PrimaryGeneratorAction.hh"
#include "DetectorConstruction.hh"
#include "ParallelWorldConstruction.hh"
#include "StackingAction.hh"
#include "SteppingAction.hh"
#include "EventAction.hh"
#include "G4RunManager.hh"
#include "TFile.h"
#include "TTree.h"

#ifdef VIS
#include "G4UImanager.hh"
#include "G4VisExecutive.hh"
#endif

int main(int,char** argv) {
  int thread = atoi(argv[1]);
  time_t seed;
  time(&seed);
  seed += atoi(argv[3]);
  cout << "Random seed : " << seed << endl;
  CLHEP::RanecuEngine *theRanGenerator = new CLHEP::RanecuEngine;
  theRanGenerator->setSeed(seed);
  CLHEP::HepRandom::setTheEngine(theRanGenerator);

  G4RunManager* runManager = new G4RunManager;
  G4String paraWorldName = "ParallelWorld";
  DetectorConstruction* det = new DetectorConstruction();
  ParallelWorldConstruction* PA = 0x0;
#ifdef REALPLAN
  G4String config = argv[2];
  if ( config!="Single_source" ) PA = new ParallelWorldConstruction(paraWorldName);
  else paraWorldName = "NoParallelWorld";
#else
  int config = atoi(argv[2]);
  if ( config!=0 ) PA = new ParallelWorldConstruction(paraWorldName);
  else paraWorldName = "NoParallelWorld";
#endif
  runManager->SetUserInitialization( new PhysicsList(paraWorldName) );
  if (PA) det->RegisterParallelWorld( PA );
  runManager->SetUserInitialization( det );
  PrimaryGeneratorAction* PG = new PrimaryGeneratorAction(thread);
  runManager->SetUserAction( PG );
  runManager->SetUserAction( new SteppingAction() );
  runManager->SetUserAction( new StackingAction() );
  EventAction *evt = new EventAction();
  runManager->SetUserAction( evt );
  runManager->Initialize();
  evt->Initialize();

  int nPhotons = PG->nPhotons;

#ifdef VIS
  G4UImanager * UImanager = G4UImanager::GetUIpointer();
  //UImanager->ApplyCommand("/run/verbose 2");
  //UImanager->ApplyCommand("/event/verbose 1");
  UImanager->ApplyCommand("/tracking/verbose 2");
  G4VisManager* visManager = new G4VisExecutive;
  visManager->Initialize();
  UImanager->ApplyCommand(Form("/control/execute %s/data/vis.mac",getenv("G4WORKDIR")));
#endif

#ifdef REALPLAN
  ifstream dwells(Form("%s.dat",config.data()));
  G4double TotalTime = PG->TotalTime;
  int d1,d2;
  float t;

  if ( config!="Single_source" ) {
    while ( dwells >> t >> d1 >> d2 ) {
      float N = int(t*float(nPhotons)/TotalTime);
      cout << t << " " << d1 << " " << d2 << " " << N << endl;
      if ( d2!=-1 ) {
        PG->SetSourceAt(d1);
        PA->SetSourceAt(d2);
        runManager->GeometryHasBeenModified();
        runManager->BeamOn(N);
        PG->SetSourceAt(d2);
        PA->SetSourceAt(d1);
        runManager->GeometryHasBeenModified();
        runManager->BeamOn(N);
      } else {
        PG->SetSourceAt(d1);
        PA->SetSourceAt(-1);
        runManager->GeometryHasBeenModified();
        runManager->BeamOn(N);
      }
    }
  } else {
    while ( dwells >> t >> d1 >> d2 ) {
      float N = int(t*float(nPhotons)/TotalTime);
      cout << t << " " << d1 << " " << N << endl;
      PG->SetSourceAt(d1);
      runManager->BeamOn(N);
    }
  }
  dwells.close();
  evt->Finalize(Form("%s/t.%d.root",config.data(),thread));

#else
  if ( config!=0 ) {
    PA->SetSourcePosition(float(config));
    runManager->GeometryHasBeenModified();
  }
  runManager->BeamOn( nPhotons );
  evt->Finalize(Form("%s/data/log.%d/%d.root",getenv("G4WORKDIR"),config,thread));
#endif

#ifdef VIS
  delete visManager;
#endif
  delete runManager;
}
