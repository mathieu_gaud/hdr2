#ifndef SteelCable_hh
#define SteelCable_hh 

#include "VSeed.hh"

class G4LogicalVolume;

class SteelCable: public VSeed {
public:
  SteelCable();
  ~SteelCable(){;}

  void Construct();
  void PlaceYourself(G4VPhysicalVolume*,G4int);

private:
  G4double outerCableRadius;
  G4double halfZCableLength;

  G4LogicalVolume* cable_log;
};

#endif
