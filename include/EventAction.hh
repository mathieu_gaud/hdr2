#ifndef EventAction_hh
#define EventAction_hh

#include "G4UserEventAction.hh"
#include "G4ThreeVector.hh"
#include <map>

class G4PhantomParameterisation;
class SteppingAction;
class TH3D;

class EventAction : public G4UserEventAction {
public:
  EventAction();
  ~EventAction();
  void BeginOfEventAction(const G4Event*);
  void EndOfEventAction(const G4Event*);
  void Initialize();
  void Finalize(G4String);
  static inline EventAction* GetInstance() { return theEventAction; }

private:
  static EventAction* theEventAction;
  G4PhantomParameterisation* par;
  SteppingAction* ste;
  map<G4int,G4double>::iterator it,it_end;

  G4ThreeVector shift;
  TH3D* Edep;
#ifdef PFR
  TH3D* Epfr[3];
#endif
};

#endif
