#ifndef PrimaryGeneratorAction_hh
#define PrimaryGeneratorAction_hh

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ThreeVector.hh"
#include "TTree.h"

using namespace std;

class G4ParticleGun;
class VSeed;
class TFile;

class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction {
public:
  PrimaryGeneratorAction(G4int);
  ~PrimaryGeneratorAction();

  static inline PrimaryGeneratorAction* GetInstance() { return theGenerator; }
  void ReadFile();
  void GeneratePrimaries(G4Event* anEvent);
  VSeed* GetEmittor() { return emittor; }
  VSeed* GetSpectator() { return spectator; }

  G4int nPhotons; // number of primary photons
  G4int nPhotonsGenerated; // number of primary photons thrown
  G4double IrradiatedEnergy;

  void SetSourceAt(G4int);
  G4double TotalTime;
  vector<G4ThreeVector> Positions;
  vector<G4ThreeVector> Directions;

private:
  static PrimaryGeneratorAction* theGenerator;
  G4int thread;
  VSeed *emittor;
  VSeed *spectator;
  G4ParticleGun* particleGun;
  G4ThreeVector Momentum;
  G4ThreeVector Position;
  G4ThreeVector SourcePosition;
  G4ThreeVector SourceDirection;

  Float_t x,y,z,theta,phi,E,px,py,pz;
  TTree *t;
  TFile *f;
};

#endif
