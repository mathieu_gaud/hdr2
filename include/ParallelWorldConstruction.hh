#ifndef ParallelWorldConstruction_hh
#define ParallelWorldConstruction_hh

#include "globals.hh"
#include "G4VUserParallelWorld.hh"

class G4VPhysicalVolume;
class G4PVPlacement;
class G4LogicalVolume;
class G4Tubs;
class VSeed;

class ParallelWorldConstruction : public G4VUserParallelWorld {
public:
  ParallelWorldConstruction(G4String& worldName);
  virtual ~ParallelWorldConstruction();
  virtual void Construct();

  //void SetSourceAt(G4int);
  //void SetSourcePosition(G4double,G4double,G4double);
  //void SetCylinderPosition(G4double,G4double,G4double,G4double,G4double);


private:
  G4VPhysicalVolume* ghostWorld;
  VSeed *spectator;

  G4LogicalVolume* cont_log;
  G4PVPlacement* cont_phys;

  G4LogicalVolume* cyl_log;
  G4PVPlacement* cyl_phys;
  G4Tubs* cyl_vol;

  G4LogicalVolume* cyl_air_log;
  G4PVPlacement* cyl_air_phys;
  G4Tubs* cyl_air_vol;

};

#endif
