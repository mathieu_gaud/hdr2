#ifndef SteppingAction_hh
#define SteppingAction_hh

#include "G4UserSteppingAction.hh"
#include <map>

class LinearTrackLengthEstimator;

class SteppingAction : public G4UserSteppingAction {
public:
  SteppingAction();
  ~SteppingAction();
  void UserSteppingAction(const G4Step*);
  static inline SteppingAction* GetInstance() { return theSteppingAction; }

  map<G4int,G4double> Edep;
#ifdef PFR
  map<G4int,G4double> Epfr[3];
  G4int pfr;
#endif

private:
  static SteppingAction* theSteppingAction;
  LinearTrackLengthEstimator* LTE;

  vector< pair<G4int,G4double> >::iterator it,it_end;
  pair<map<G4int,G4double>::iterator,bool> ret;
};

#endif
