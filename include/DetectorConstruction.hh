#ifndef DetectorConstruction_hh
#define DetectorConstruction_hh

#include "G4VUserDetectorConstruction.hh"
#include "G4ThreeVector.hh"

class G4PhantomParameterisation;
class G4VPhysicalVolume;
class VSeed;

class DetectorConstruction : public G4VUserDetectorConstruction {
public:
  DetectorConstruction();
  ~DetectorConstruction();

  G4VPhysicalVolume* Construct();
  static inline DetectorConstruction* GetInstance() { return theDetector; }
  G4PhantomParameterisation* GetParam() { return param; }
  G4int NbinsX,NbinsY,NbinsZ;
  G4double Xmax,Xmin,halfX;
  G4double Ymax,Ymin,halfY;
  G4double Zmin,Zmax,halfZ;
  G4ThreeVector shift;

private:
  static DetectorConstruction* theDetector;
  G4PhantomParameterisation* param;
  VSeed *s;
};

#endif
