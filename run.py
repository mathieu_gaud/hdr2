#!/usr/bin/env python
import os,sys,time
from random import randint
from threading import Thread
import subprocess as sp
import multiprocessing
nthreads = multiprocessing.cpu_count()

# ==================================================================
# Configuration
# ==================================================================
#i0 = int(sys.argv[1])
#config = sys.argv[2]
#os.chdir(sys.argv[3])
i0=0

# ==================================================================
# Threads
# ==================================================================
#os.chdir(os.environ['G4WORKDIR'])
#DIR = 'data/log.%d/'%(A['CONFIG'])
#if not os.path.exists(DIR): os.mkdir(DIR)


class Run(Thread):
  def __init__(self,i):
    Thread.__init__(self)
    self.thread = i
  def run(self):
    cmd = '/Users/mathieugaudreault/development/G4MC/bin/Darwin-g++/HDR2 %d %d' % (self.thread,randint(0,1e15))
    #cmd = 'HDR2 %d %s %d' % (self.thread,config,12345678)
    #cmd = 'HDR2 %d %d %d' % (self.thread,config,randint(0,1e15))
    print cmd
    self.P = sp.Popen(cmd,shell=True,stdout=sp.PIPE,stderr=sp.PIPE,stdin=sp.PIPE)
    self.out = self.P.communicate()

print 'Creating %d threads' % (nthreads)
threads = range(i0,i0+nthreads)
th = []
for t in threads: th.append(Run(t))
for t in th: t.start(); time.sleep(0.1)
for t in th: t.join()
