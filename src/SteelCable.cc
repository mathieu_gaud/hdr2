#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4VisAttributes.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PhysicalVolumeStore.hh" 
#include "G4PVPlacement.hh"
#include "G4NistManager.hh"
#include "G4Element.hh"
#include "SteelCable.hh"

SteelCable::SteelCable():VSeed("SteelCable") { 
  //################################################################
  // Seed dimensions
  //################################################################
  outerCableRadius = 0.35*mm;
  halfZCableLength = 40*cm;

  VSeed::OutRadius = outerCableRadius;
  VSeed::ZHalfLength = halfZCableLength;
}

void SteelCable::Construct() {
  //################################################################
  // Define the seed materials
  //################################################################
  G4int nel;
  G4double density, a, z, pct;
  G4String name, symbol;

  G4Element* Fe = new G4Element(name="Iron"     ,symbol="Fe", z=26.0, a=55.845000*g/mole);
  G4Element* Cr = new G4Element(name="Chromium" ,symbol="Cr", z=24.0, a=51.996100*g/mole);
  G4Element* Ni = new G4Element(name="Nickel"   ,symbol="Ni", z=28.0, a=58.693400*g/mole);
  G4Element* Mn = new G4Element(name="Manganese",symbol="Mn", z=25.0, a=54.938049*g/mole);
  G4Element* Si = new G4Element(name="Silicium" ,symbol="Si", z=14.0, a=28.085500*g/mole);
  G4Element* C  = new G4Element(name="Carbon"   ,symbol="C" , z= 6.0, a=12.010700*g/mole);

  // Stainless Steel 304
  G4Material* mat_Steel = new G4Material("Steel",density=8.0*g/cm3,nel=6);
  mat_Steel->AddElement(Fe,pct=0.6792);
  mat_Steel->AddElement(Cr,pct=0.1900);
  mat_Steel->AddElement(Ni,pct=0.1000);
  mat_Steel->AddElement(Mn,pct=0.0200);
  mat_Steel->AddElement(Si,pct=0.0100);
  mat_Steel->AddElement(C ,pct=0.0008);

  // Stainless Steel 304 Cable
  G4Tubs* cable_vol = new G4Tubs("cable_vol",0.,outerCableRadius,halfZCableLength,0.,twopi);
  cable_log = new G4LogicalVolume(cable_vol,mat_Steel,"cable_log");
  G4VisAttributes* cable_att = new G4VisAttributes();
  cable_att->SetColor(1,1,1,0.5);
  cable_att->SetForceSolid(true);
  cable_att->SetVisibility(true);
  cable_log->SetVisAttributes(cable_att);

  VSeed::OuterShell = cable_vol;
}

void SteelCable::PlaceYourself(G4VPhysicalVolume* parent,G4int copyNo) {
  if ( !VSeed::OuterShell ) Construct();
  new G4PVPlacement(0,G4ThreeVector(),"cable_phys",cable_log,parent,false,copyNo);
}
