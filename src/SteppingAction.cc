#include "G4Step.hh"
#include "G4VProcess.hh"
#include "G4RegularNavigationHelper.hh"
#include "DetectorConstruction.hh"
#include "G4PhantomParameterisation.hh"
#include "LinearTrackLengthEstimator.hh"
#include "SteppingAction.hh"

SteppingAction *SteppingAction::theSteppingAction=NULL;

SteppingAction::~SteppingAction() {
  theSteppingAction=NULL;
  delete LTE;
}

SteppingAction::SteppingAction() {
  theSteppingAction=this;
  LTE = new LinearTrackLengthEstimator();
}

void SteppingAction::UserSteppingAction(const G4Step* aStep) {
  G4Track *tr = aStep->GetTrack();
  G4String proc = aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName();

  if ( tr->GetVolume()->GetName()!="phantom_phys" ) return;

  G4double en = aStep->GetPreStepPoint()->GetKineticEnergy();
  G4double dose = LTE->MuenRho(tr->GetMaterial(),en)*en;

  vector< pair<G4int,G4double> > Steps = G4RegularNavigationHelper::theStepLengths;
  it = Steps.begin();
  it_end = Steps.end();

  G4double CumulStepLength = 0.;
  G4double StepLength = aStep->GetStepLength();
  while ( fabs(StepLength-CumulStepLength)>1e-6 && it!=it_end ) {

   if ( CumulStepLength+it->second > StepLength ) it->second = StepLength - CumulStepLength;
   CumulStepLength += it->second;
   it->second *= dose;

   ret = Edep.insert(*it);
   if ( !ret.second ) Edep[it->first] += it->second;
#ifdef PFR
   ret = Epfr[_pfr].insert(*it);
   if ( !ret.second ) Epfr[_pfr][it->first] += it->second;
#endif
   ++it;
 }
}

