#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4Gamma.hh"
#include "Randomize.hh"
#include "FlexiSourceIr192.hh"
#include "MicroSelectronV2Ir192.hh"
#include "FlexiSourceYb169.hh"
#include "SteelCable.hh"
#include "TFile.h"
#include <fstream>
#include <algorithm>
#include "PrimaryGeneratorAction.hh"

PrimaryGeneratorAction* PrimaryGeneratorAction::theGenerator = NULL;

PrimaryGeneratorAction::~PrimaryGeneratorAction() {
  delete particleGun;
  theGenerator = NULL;
}

PrimaryGeneratorAction::PrimaryGeneratorAction(G4int _thread) {
  theGenerator = this;

  thread = _thread;
  nPhotonsGenerated = 0;
  IrradiatedEnergy = 0.;
  
  ReadFile();

  emittor = new MicroSelectronV2Ir192(); 
  emittor->Construct();

  SetSourceAt(0);

  spectator = emittor; 

  particleGun = new G4ParticleGun();
  particleGun->SetParticleDefinition(G4Gamma::Gamma());

  // Handle to PhaseSpace
  f = new TFile(Form("%s/data/PhaseSpace/%s/%d.root",getenv("G4WORKDIR"),emittor->GetName().data(),thread),"read");
  t = (TTree*)f->Get("phase");
  t->SetBranchAddress("x",&x);
  t->SetBranchAddress("y",&y);
  t->SetBranchAddress("z",&z);
  t->SetBranchAddress("theta",&theta);
  t->SetBranchAddress("phi",&phi);
  t->SetBranchAddress("E",&E);
  nPhotons = 20000000;


}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent) {
  t->GetEntry( nPhotonsGenerated );

  Position = G4ThreeVector(x,y,z);
  Momentum.setRThetaPhi(1.,theta,phi);
  particleGun->SetParticlePosition( Position );
  particleGun->SetParticleMomentumDirection( Momentum );
  particleGun->SetParticleEnergy( E );
  particleGun->GeneratePrimaryVertex( anEvent );

  IrradiatedEnergy += E;
  nPhotonsGenerated++;
}

void PrimaryGeneratorAction::SetSourceAt(G4int i) {
cout << Positions[i] << endl;
  SourcePosition = Positions[i];
  SourceDirection = Directions[i];
}

void PrimaryGeneratorAction::ReadFile() {
  ifstream dwells("/Users/mathieugaudreault/development/G4MC/data/log.Single_source/dwells.dat");
  TotalTime = 0;

  while ( dwells >> x >> y >> z >> E >> px >> py >> pz ) {
    Positions.push_back(G4ThreeVector(x,y,z));
    Directions.push_back(G4ThreeVector(px,py,pz));
    TotalTime += E;
  }

  dwells.close();
}
