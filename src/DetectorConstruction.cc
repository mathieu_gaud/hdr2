#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PhantomParameterisation.hh"
#include "G4PVParameterised.hh"
#include "G4VisAttributes.hh"
#include "G4SubtractionSolid.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4NistManager.hh"
#include "G4Material.hh"
#include "TFile.h"
#include "TH3D.h"
#include "DetectorConstruction.hh"

using namespace std;

DetectorConstruction* DetectorConstruction::theDetector=NULL;
DetectorConstruction::~DetectorConstruction() {
  theDetector=NULL;
}

DetectorConstruction::DetectorConstruction() { 
  theDetector=this;
//Dimension de la boite :
  TH3D* Densities = new TH3D("Densities","",200,-10,10,200,-10,10,200,-10,10);//,99,+0.5,99.5,29,-14.5,14.5,29,-14.5,14.5
//,200,-10,10,200,-10,10,200,-10,10
  NbinsX = Densities->GetNbinsX();
  NbinsY = Densities->GetNbinsY();
  NbinsZ = Densities->GetNbinsZ();
  Xmax = Densities->GetXaxis()->GetXmax();
  Ymax = Densities->GetYaxis()->GetXmax();
  Zmax = Densities->GetZaxis()->GetXmax();
  Xmin = Densities->GetXaxis()->GetXmin();
  Ymin = Densities->GetYaxis()->GetXmin();
  Zmin = Densities->GetZaxis()->GetXmin();
  halfX = 0.5*Densities->GetXaxis()->GetBinWidth(1);
  halfY = 0.5*Densities->GetYaxis()->GetBinWidth(1);
  halfZ = 0.5*Densities->GetZaxis()->GetBinWidth(1);
  shift = G4ThreeVector((Xmax+Xmin)/2.,(Ymax+Ymin)/2.,(Zmax+Zmin)/2.);
}

G4VPhysicalVolume* DetectorConstruction::Construct() {
  G4cout << "DetectorConstruction::Construct" << G4endl;
  // Define Materials 
  G4NistManager* man = G4NistManager::Instance();
  //Water = man->FindOrBuildMaterial("G4_WATER");
  G4Element* elH = man->FindOrBuildElement(1);
  G4Element* elO = man->FindOrBuildElement(8);
  G4Material* Water = new G4Material("Water",0.998*g/cm3,2);
  Water->AddElement(elH,2);
  Water->AddElement(elO,1);

  // Cubic water world
  G4double hall_side = 50*cm;
  G4Box* hall_vol = new G4Box("hall_vol",hall_side,hall_side,hall_side);
  G4LogicalVolume* hall_log = new G4LogicalVolume(hall_vol,Water,"hall_log");
  G4PVPlacement* hall_phys = new G4PVPlacement(0,G4ThreeVector(),hall_log,"hall_phys",0,false,0);
  G4VisAttributes* hall_att = new G4VisAttributes(G4Colour(1,0,0));
  hall_att->SetVisibility(true);
  hall_log->SetVisAttributes(hall_att);

  // Container box
  G4Box* cont_vol = new G4Box("cont_vol",NbinsX*halfX,NbinsY*halfY,NbinsZ*halfZ);
  G4LogicalVolume* cont_log = new G4LogicalVolume(cont_vol,Water,"cont_log",0,0,0);
  G4PVPlacement* cont_phys = new G4PVPlacement(0,shift,cont_log,"cont_phys",hall_log,false,0);
  G4VisAttributes* cont_att = new G4VisAttributes(G4Colour(0,0,1));
  cont_att->SetVisibility(true);
  cont_log->SetVisAttributes(cont_att);

  G4Box* phantomVoxel_vol = new G4Box("phantomVoxel_vol",halfX,halfY,halfZ);
  G4LogicalVolume* phantomVoxel_log = new G4LogicalVolume(phantomVoxel_vol,Water,"phantomVoxel_log",0,0,0);
  G4VisAttributes* phantomVoxel_att = new G4VisAttributes();
  phantomVoxel_att->SetVisibility(false);
  phantomVoxel_log->SetVisAttributes(phantomVoxel_att);

  // Set materials
  size_t* materialIDs = new size_t[NbinsX*NbinsY*NbinsZ];
  unsigned int p = 0;
  for (G4int k=0;k<NbinsZ;k++) {
    for (G4int j=0;j<NbinsY;j++) {
      for (G4int i=0;i<NbinsX;i++) { materialIDs[p] = 0; p++; }
    }
  }

  // Parameterisation of the human phantom
  G4cout << "Make a parameterised phantom" << G4endl;
  param = new G4PhantomParameterisation();
  param->SetVoxelDimensions(halfX,halfY,halfZ);
  param->SetNoVoxel(NbinsX,NbinsY,NbinsZ);
  vector<G4Material*> theMaterials; theMaterials.push_back(Water);
  param->SetMaterials(theMaterials);
  param->SetMaterialIndices(materialIDs);
  param->SetSkipEqualMaterials(true);
  param->BuildContainerSolid(cont_phys);
  param->CheckVoxelsFillContainer(cont_vol->GetXHalfLength(),cont_vol->GetYHalfLength(),cont_vol->GetZHalfLength());

  G4PVParameterised* phantom_phys = new G4PVParameterised("phantom_phys",phantomVoxel_log,cont_log,kZAxis,param->GetNoVoxel(),param);
  phantom_phys->SetRegularStructureId(1);

  return hall_phys;
}
