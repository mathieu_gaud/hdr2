#include "ParallelWorldConstruction.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "PrimaryGeneratorAction.hh"
#include "G4PVPlacement.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "MicroSelectronV2Ir192.hh"
#include "G4Tubs.hh"
#include "G4Material.hh"
#include "VSeed.hh"
#include "G4NistManager.hh"

ParallelWorldConstruction::ParallelWorldConstruction(G4String& worldName) :G4VUserParallelWorld(worldName) {;}

ParallelWorldConstruction::~ParallelWorldConstruction() {;}

void ParallelWorldConstruction::Construct() {
  ghostWorld = GetWorld();
// Place une source physique MicroSelectron à la position (0,0,0)
  spectator = (MicroSelectronV2Ir192*)PrimaryGeneratorAction::GetInstance()->GetSpectator();
  G4Tubs* cont_vol = new G4Tubs("cont_vol",0.,spectator->OutRadius,spectator->ZHalfLength,0.,twopi);
  cont_log = new G4LogicalVolume(cont_vol,0,"cont_log",0,0,0);
  cont_phys = new G4PVPlacement(G4Transform3D(G4RotationMatrix(),G4ThreeVector(0.,0.,0.)),"cont_phys",cont_log,ghostWorld,false,0);
  spectator->PlaceYourself(cont_phys,0);
// Place un cylindre de métal remplit d'air à la position (0,0,0)
  G4double outRadius=1.0*mm; 
  G4double inRadius=0.46*mm;
  G4double lengthCylinder=50.0*cm;
  G4NistManager* man = G4NistManager::Instance();
  G4Element* Fe = man->FindOrBuildElement("Fe");
  G4Element* Si = man->FindOrBuildElement("Si"); 
  G4Element* Cr = man->FindOrBuildElement("Cr");
  G4Element* Mn = man->FindOrBuildElement("Mn");
  G4Element* Ni = man->FindOrBuildElement("Ni");
  G4Material* StainlessSteel = new G4Material("StainlessSteel",8.02*g/cm3, 5);
  StainlessSteel->AddElement(Fe, 0.6800);
  StainlessSteel->AddElement(Cr, 0.1700);
  StainlessSteel->AddElement(Ni, 0.1200);
  StainlessSteel->AddElement(Mn, 0.0200);
  StainlessSteel->AddElement(Si, 0.0100);
  G4Material* Air  = man->FindOrBuildMaterial("G4_AIR");

  cyl_air_vol = new G4Tubs("cyl_air_vol",0,inRadius,lengthCylinder,0.,twopi);
  cyl_air_log = new G4LogicalVolume(cyl_air_vol,Air,"cyl_air_log",0,0,0);
  cyl_air_phys = new G4PVPlacement(G4Transform3D(G4RotationMatrix(),G4ThreeVector(0.,0.,0.)),"cyl_air_phys",cyl_air_log,ghostWorld,false,0);

  cyl_vol = new G4Tubs("cyl_vol",inRadius,outRadius,lengthCylinder,0.,twopi);
  cyl_log = new G4LogicalVolume(cyl_vol,StainlessSteel,"cyl_log",0,0,0);
  cyl_phys = new G4PVPlacement(G4Transform3D(G4RotationMatrix(),G4ThreeVector(0.,0.,0.)),"cyl_phys",cyl_log,ghostWorld,false,0);

}
