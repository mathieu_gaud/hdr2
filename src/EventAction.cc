#include "G4PhantomParameterisation.hh"
#include "DetectorConstruction.hh"
#include "ParallelWorldConstruction.hh"
#include "PrimaryGeneratorAction.hh"
#include "SteppingAction.hh"
#include "TFile.h"
#include "TMath.h"
#include "TH3D.h"
#include "VSeed.hh"
#include "EventAction.hh"

EventAction *EventAction::theEventAction=NULL;

EventAction::EventAction() {}
EventAction::~EventAction() {
  theEventAction=NULL;
}

void EventAction::Initialize() {
  DetectorConstruction* det = DetectorConstruction::GetInstance();
  par = DetectorConstruction::GetInstance()->GetParam();
  ste = SteppingAction::GetInstance();
  shift = det->shift;

  Edep = new TH3D("Edep","",det->NbinsX,det->Xmin,det->Xmax,det->NbinsY,det->Ymin,det->Ymax,det->NbinsZ,det->Zmin,det->Zmax);
  Edep->Sumw2();
}

void EventAction::BeginOfEventAction(const G4Event*) {ste->Edep.clear();}

void EventAction::Finalize(G4String rootfile) {
  PrimaryGeneratorAction* gen = PrimaryGeneratorAction::GetInstance();
  TFile f(rootfile,"recreate");
  Edep->SetBinContent(0,gen->IrradiatedEnergy);
  Edep->SetBinContent(1,gen->nPhotonsGenerated);
  Edep->Write("tot");
  delete Edep;
  f.Close();
}

void EventAction::EndOfEventAction(const G4Event*) {
  it     = ste->Edep.begin();
  it_end = ste->Edep.end();
  while ( it!=it_end ) {
    G4ThreeVector pos = par->GetTranslation(it->first) + shift;
    Edep->Fill(pos[0],pos[1],pos[2],it->second);
    ++it;
  }

}
