name := $(shell basename $(PWD))

G4TARGET := $(name)
G4EXLIB := true
G4_NO_VERBOSE := true
G4LIB_USE_EXPAT := true
CPPFLAGS += $(shell root-config --cflags) -I$(G4WORKDIR)/$(G4TARGET)/include -I$(G4WORKDIR)/CalibSources/include
EXTRALIBS:= $(shell root-config --ldflags) $(shell root-config --libs) -L$(G4WORKDIR)/tmp/$(G4SYSTEM)/$(G4TARGET) -l$(G4TARGET) -L$(G4WORKDIR)/tmp/$(G4SYSTEM)/CalibSources -lCalibSources

ifdef REALPLAN
CPPFLAGS += -DREALPLAN
endif
ifdef VIS
CPPFLAGS += -DVIS
endif

.PHONY: all
all: lib bin

include $(G4INSTALL)/config/binmake.gmk
