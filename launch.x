mkdir /Users/mathieugaudreault/development/G4MC/data/results/radius_out_new/radius_out_1
sed -is " s/outRadius=1.0/outRadius=1.0/g " src/ParallelWorldConstruction.cc
sed -is " s/radius_out_1/radius_out_1/g " HDR2.cc
make clean ; make 
./run.py

mkdir /Users/mathieugaudreault/development/G4MC/data/results/radius_out_new/radius_out_15
sed -is " s/outRadius=1.0/outRadius=1.5/g " src/ParallelWorldConstruction.cc
sed -is " s/radius_out_1/radius_out_15/g " HDR2.cc
make clean ; make 
./run.py

mkdir /Users/mathieugaudreault/development/G4MC/data/results/radius_out_new/radius_out_2
sed -is " s/outRadius=1.5/outRadius=2.0/g " src/ParallelWorldConstruction.cc
sed -is " s/radius_out_15/radius_out_2/g " HDR2.cc
make clean ; make 
./run.py

mkdir /Users/mathieugaudreault/development/G4MC/data/results/radius_out_new/radius_out_25
sed -is " s/outRadius=2.0/outRadius=2.5/g " src/ParallelWorldConstruction.cc
sed -is " s/radius_out_2/radius_out_25/g " HDR2.cc
make clean ; make 
./run.py

mkdir /Users/mathieugaudreault/development/G4MC/data/results/radius_out_new/radius_out_3
sed -is " s/outRadius=2.5/outRadius=3.0/g " src/ParallelWorldConstruction.cc
sed -is " s/radius_out_25/radius_out_3/g " HDR2.cc
make clean ; make 
./run.py



